# StreamLined

StreamLined project for ITIS 4155

## Required softwares

This applications runs on NodeJS, you will need to install NodeJS here https://nodejs.org/en/download/

Download and install NodeJS for your Operating System.

## Installing StreamLined

Open a command prompt/git bash.
Enter the directory on your PC that you wish to have StreamLined on and run this command in the command prompt:
```
git clone https://gitlab.com/fish_KING_/project.git
```

## Running the application

Enter your cloned project in explorer and go into the folder named "StreamLined".
You will know if you are in the correct directory if there is a file named "app.js" in this directory.

Make sure to cd into the project/StreamLined/ directory in your command prompt (Directory with app.js in it).

After entering the project/StreamLined/ directory in your command prompt,
Run the command 
```
nodemon app
```
The application should run and be set on port 443.

After that, go onto your browser and go to "http://localhost:443" in your search bar.
StreamLined should display after doing this.

